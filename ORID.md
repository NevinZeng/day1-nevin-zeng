## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

    This morning, the TW teacher introduced to us the training plan for the next four weeks, so that we could understand what we would learn through these four weeks. Then we had an ice-breaking exercise, the purpose of which was to get the students to know each other quickly.  
    In the afternoon, we first learned what concept maps are and how to draw them, and conducted a training. Each group selected a topic and had a group discussion. Finally, we drew corresponding concept maps on the whiteboard, and then we showed case to the whole class. Finally, I learned what standing meeting and ORID are.

    I was most impressed when I participated in the ice-breaking operation and discussed the concept map in the group.

## R (Reflective): Please use one word to express your feelings about today's class.

    Fruitful.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

    I think group discussion can greatly promote team cooperation and thinking ability, and team members can express their own opinions and combine different views to finally give a relatively correct result.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

    Through today's learning, the future daily newspaper can be written in the way of OCID.
